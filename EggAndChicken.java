 class EggAndChicken extends Thread {
    private String name;
    private int count;
    private EggAndChicken adversary;

    EggAndChicken(String name)
    {this.name = name;}

    void setOpponent(EggAndChicken opponent) {
        adversary = opponent;
    }

    public void run() {
        while ((count++) < 1000) {
            System.out.println("ШАГ:" + count +" " + name);
        }

        if (!adversary.isAlive()) {
            System.out.printf("Побеждает %s", name);
        }
    }
}
